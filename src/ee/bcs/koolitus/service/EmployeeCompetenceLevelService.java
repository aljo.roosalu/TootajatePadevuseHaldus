package ee.bcs.koolitus.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import ee.bsc.koolitus.bean.EmployeeCompetenceLevel;



public class EmployeeCompetenceLevelService {



	public EmployeeCompetenceLevel addEmployeeCompetenceLevel(EmployeeCompetenceLevel employeeCompetenceLevel) {
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate("INSERT INTO employeescompetences.employee_competence_level(id_employee, id_competence, id_level) VALUES ('"
					+ employeeCompetenceLevel.getIdEmployee() + "', " + employeeCompetenceLevel.getIdCompetence() + 
					", " + employeeCompetenceLevel.getIdLevel() + ");");

		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return employeeCompetenceLevel;
	}
}
