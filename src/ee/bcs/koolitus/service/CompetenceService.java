package ee.bcs.koolitus.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bsc.koolitus.bean.Competence;
import ee.bsc.koolitus.bean.Level;

public class CompetenceService {

	public List<Competence> getAllCompetences() {
		List<Competence> listOfCompetences = new ArrayList<>();

		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM employeescompetences.competences;");) {
			while (resultSet.next()) {
				Competence competence = new Competence(resultSet.getInt("id_competences"), resultSet.getString("name"),
						resultSet.getString("description"));

				listOfCompetences.add(competence);

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return listOfCompetences;
	}

	public List<Competence> getCompetenceByEmployee(int employeeId) {
		List<Competence> listOfCompetences = new ArrayList<>();

		LevelService levelService = new LevelService();
		List<Level> levels = levelService.getAllLevels();

		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"SELECT comp.id_competences as comp_id, comp.name as comp_name, comp.description as comp_desc, lev.* FROM (employeescompetences.employee_competence_level emp inner join employeescompetences.competences comp on emp.id_competence = comp.id_competences) inner join employeescompetences.levels lev on emp.id_level = lev.id_level WHERE emp.id_employee="
								+ employeeId + ";");) {
			while (resultSet.next()) {
				Competence competence = new Competence(resultSet.getInt("comp_id"), resultSet.getString("comp_name"),
						resultSet.getString("comp_desc"));
				for (Level level : levels) {
					if (level.getId() == resultSet.getInt("id_level")) {
						competence.setLevel(level);
					}

				}

				listOfCompetences.add(competence);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return listOfCompetences;
	}

	public Competence addCompetence(Competence competence) {
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate("INSERT INTO employeescompetences.competences " + "(name, description) VALUES ('"
					+ competence.getName() + "', '" + competence.getDescription() + "');");

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return competence;
	}
}
