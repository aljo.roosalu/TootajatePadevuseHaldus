package ee.bcs.koolitus.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bsc.koolitus.bean.Level;

public class LevelService {
	
	public List<Level> getAllLevels() {
		List<Level> listOfLevels = new ArrayList<>();
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM employeescompetences.levels;");) {
			while (resultSet.next()) {
				listOfLevels.add(new Level(resultSet.getInt("id_level"), resultSet.getString("name"),
						resultSet.getString("description")));
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return listOfLevels;
	}
	

}
