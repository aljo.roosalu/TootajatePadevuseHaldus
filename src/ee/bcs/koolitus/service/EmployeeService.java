package ee.bcs.koolitus.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import java.util.List;

import ee.bsc.koolitus.bean.Employee;


public class EmployeeService {

	public EmployeeService() {
	}

	public List<Employee> getAllEmployees() {
		List<Employee> listOfEmployees = new ArrayList<>();
		
		
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM employeescompetences.employees;");) {
			while (resultSet.next()) {
				Employee employee = new Employee(resultSet.getInt("id_employee"), resultSet.getString("username"), resultSet.getString("first_name"),
						resultSet.getString("last_name"), resultSet.getString("country"),
						resultSet.getString("id_code"), resultSet.getString("job_title"),
						resultSet.getString("education"));
				
				listOfEmployees.add(employee);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return listOfEmployees;
	}
	
	public List<Employee> checkEmployee(String username, String password) {
		List<Employee> listOfEmployees = new ArrayList<>();
		Connection connection = DBConnectionHandling.createConnection();
		
		try {
			Statement statement = (Statement) connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM employees WHERE username='" + username + "' AND password='" + password + "';");
			
			while (resultSet.next()) {
					Employee employee = new Employee(resultSet.getInt("id_employee"), resultSet.getString("username"), resultSet.getString("first_name"),
							resultSet.getString("last_name"), resultSet.getString("country"),
							resultSet.getString("id_code"), resultSet.getString("job_title"),
							resultSet.getString("education"));
					listOfEmployees.add(employee);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		DBConnectionHandling.closeConnection(connection);
		return listOfEmployees;
	}

	public Employee getEmployeeById(int idEmployee) {
		Employee employee = null;
		
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement
						.executeQuery("SELECT * FROM employeescompetences.employees WHERE id_employee=" + idEmployee + ";");) {
			while (resultSet.next()) {
				employee = new Employee(resultSet.getInt("id_employee"), resultSet.getString("first_name"),
						resultSet.getString("last_name"), resultSet.getString("country"),
						resultSet.getString("id_code"), resultSet.getString("job_title"),
						resultSet.getString("education"));
				
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return employee;
	}

	public Employee addEmployee(Employee employee) {
		Connection connection = DBConnectionHandling.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate("INSERT INTO employeescompetences.employees "
					+ "(first_name, last_name, country, id_code, job_title, education) VALUES ('"
					+ employee.getFirstName() + "', '" + employee.getLastName() + "', '" + employee.getCountry()
					+ "', '" + employee.getPersonalIdentificator() + "', '" + employee.getJobTitle() + "', '"
					+ employee.getEducation() + "');");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnectionHandling.closeConnection(connection);
		return employee;
	}

}
