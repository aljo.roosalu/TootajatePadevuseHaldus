package ee.bsc.koolitus.bean;


public class Employee {

	private int idEmployee;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String country;
	private String personalIdentificator;
	private String jobTitle;
	private String education;

	public Employee() {

	}

	public Employee(int idEmployee, String username, String firstName, String lastName, String country,
			String personalIdentificator, String jobTitle, String education) {
		this.idEmployee = idEmployee;
		this.firstName = firstName;
		this.lastName = lastName;
		this.country = country;
		this.personalIdentificator = personalIdentificator;
		this.jobTitle = jobTitle;
		this.education = education;
		this.username = username;

	}

	public Employee(int idEmployee, String firstName, String lastName, String country, String personalIdentificator,
			String jobTitle, String education) {
		super();
		this.idEmployee = idEmployee;
		this.firstName = firstName;
		this.lastName = lastName;
		this.country = country;
		this.personalIdentificator = personalIdentificator;
		this.jobTitle = jobTitle;
		this.education = education;
	}

	public Employee(int idEmployee, String country, String personalIdentificator, String jobTitle, String education) {

		this.idEmployee = idEmployee;
		this.country = country;
		this.personalIdentificator = personalIdentificator;
		this.jobTitle = jobTitle;
		this.education = education;
	}

	public String getCountry() {
		return country;
	}

	public Employee setCountry(String country) {
		this.country = country;
		return this;
	}

	public String getPersonalIdentificator() {
		return personalIdentificator;
	}

	public Employee setPersonalIdentificator(String personalIdentificator) {
		this.personalIdentificator = personalIdentificator;
		return this;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public Employee setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
		return this;
	}

	public String getEducation() {
		return education;
	}

	public Employee setEducation(String education) {
		this.education = education;
		return this;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(int idEmployee) {
		this.idEmployee = idEmployee;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Employee [id=" + idEmployee + ", firstName=" + firstName + ", lastName=" + lastName + ", country="
				+ country + ", personalIdentificator=" + personalIdentificator + ", jobTitle=" + jobTitle
				+ ", education=" + education + "]";
	}

}
