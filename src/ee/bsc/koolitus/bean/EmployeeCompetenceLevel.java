package ee.bsc.koolitus.bean;

public class EmployeeCompetenceLevel {

	private int idCompetence;
	private int idEmployee;
	private int idLevel;

	public EmployeeCompetenceLevel() {

	}

	public EmployeeCompetenceLevel(int idEmployee, int idCompetence, int idLevel) {
		super();
		this.idEmployee = idEmployee;
		this.idCompetence = idCompetence;
		this.idLevel = idLevel;
	}

	public int getIdCompetence() {
		return idCompetence;
	}

	public void setIdCompetence(int idCompetence) {
		this.idCompetence = idCompetence;
	}

	public int getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(int idEmployee) {
		this.idEmployee = idEmployee;
	}

	public int getIdLevel() {
		return idLevel;
	}

	public void setIdLevel(int idLevel) {
		this.idLevel = idLevel;
	}
	
}

