package ee.bsc.koolitus.bean;

public class Competence {
	private int idCompetence;
	private int idEmployee;
	private String name;
	private String description;
	private int id;
	private Level level;

	public Competence(int idCompetence, int id) {
		this.idCompetence = idCompetence;
		this.id = id;
	}

	public Competence(int idCompetence, String name, String description) {
		this.idCompetence = idCompetence;
		this.name = name;
		this.description = description;
	}

	public Competence() {

	}

	public int getIdCompetence() {
		return idCompetence;
	}

	public void setIdCompetence(int idCompetence) {
		this.idCompetence = idCompetence;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(int idEmployee) {
		this.idEmployee = idEmployee;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

}
