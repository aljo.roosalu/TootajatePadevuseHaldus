package ee.bsc.koolitus.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;



import ee.bcs.koolitus.service.CompetenceService;

import ee.bsc.koolitus.bean.Competence;





@Path("/competences")
public class CompetenceController {
	CompetenceService competenceService = new CompetenceService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Competence> getCompetences() {

		List<Competence> listOfCompetences = competenceService.getAllCompetences();
		return listOfCompetences;
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Competence> getCompetenceByEmployee(@PathParam("id") int employeeId) {
		return competenceService.getCompetenceByEmployee(employeeId);
	}

	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Competence addCompetence(Competence competence) {
		return competenceService.addCompetence(competence);
	}

}
