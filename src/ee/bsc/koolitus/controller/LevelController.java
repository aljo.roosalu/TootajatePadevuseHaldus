package ee.bsc.koolitus.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import ee.bcs.koolitus.service.LevelService;
import ee.bsc.koolitus.bean.Level;

@Path("/levels")
public class LevelController {
	LevelService levelService = new LevelService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Level> getlevels() {

		List<Level> listOfLevels = levelService.getAllLevels();
		return listOfLevels;
	}

}	