package ee.bsc.koolitus.controller;



import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import ee.bcs.koolitus.service.EmployeeCompetenceLevelService;
import ee.bsc.koolitus.bean.EmployeeCompetenceLevel;

@Path("/commontable")
public class EmployeeCompetenceLevelController {
	EmployeeCompetenceLevelService employeeCompetenceLevelService = new EmployeeCompetenceLevelService();

	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public EmployeeCompetenceLevel addEmployeeCompetenceLevel(EmployeeCompetenceLevel employeeCompetenceLevel) {
		return employeeCompetenceLevelService.addEmployeeCompetenceLevel(employeeCompetenceLevel);
	}

}
